﻿using System;
using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.Platforms.Android.Views;
using TestApp.ViewModels;

namespace TestApp.Droid.Views
{
    [Activity(
        MainLauncher = true,
        Theme = "@style/AppTheme",
        Label = "Persons")]
    public class PersonsListView: MvxAppCompatActivity<PersonsListViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.PersonsListView);

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
        }
    }
}

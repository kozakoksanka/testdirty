﻿using System;
using System.Collections.Generic;
using TestApp.Models;

namespace TestApp.Services.Interfaces
{
    public interface IPersonsService
    {
        List<Person> GetPersons();
    }
}
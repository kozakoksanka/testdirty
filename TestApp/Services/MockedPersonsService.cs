﻿using System;
using System.Collections.Generic;
using TestApp.Models;
using TestApp.Services.Interfaces;

namespace TestApp.Services
{
    public class MockedPersonsService: IPersonsService
    {
        public List<Person> GetPersons()
        {
            return new List<Person>
            {
                new Person("Monica", "Bellucci", "https://g2.delphi.lv/images/pix/768x438/3Qg7wV1QlPg/monika-bellucci-45037652.jpg", "Manger", "monica.belucci@gmai.com", "+2805235242"),
                new Person("Eva", "Longoria", "https://storage.googleapis.com/afs-prod/media/media:c08c3b8a01f84fe3a3c9bbd2b312b867/800.jpeg", "Vice presidents", "eva.longoria@gmail.com", "+78035789634"),
                new Person("Ryan", "Gosling", "https://media.concreteplayground.com/content/uploads/2015/02/ryan-gosling.jpg", "SA", "ryangosling.gmail.com", "+7852021516"),
                new Person("Monica", "Bellucci", "https://g2.delphi.lv/images/pix/768x438/3Qg7wV1QlPg/monika-bellucci-45037652.jpg", "Lead Software", "monica.belucci@gmai.com", "+2805235242"),
                new Person("Eva", "Longoria", "https://storage.googleapis.com/afs-prod/media/media:c08c3b8a01f84fe3a3c9bbd2b312b867/800.jpeg", "Lead QA", "eva.longoria@gmail.com",  "+78035789634"),
                new Person("Ryan", "Gosling", "https://media.concreteplayground.com/content/uploads/2015/02/ryan-gosling.jpg", "BA", "ryangosling.gmail.com", "+7852021516")
            };
        }
    }
}

﻿using System;
using MvvmCross;
using MvvmCross.ViewModels;
using TestApp.Services;
using TestApp.Services.Interfaces;
using TestApp.ViewModels;

namespace TestApp
{
    public class App: MvxApplication
    {
        public override void Initialize()
        {
            base.Initialize();
            Mvx.IoCProvider.RegisterType<IPersonsService, MockedPersonsService>();
            RegisterAppStart<PersonsListViewModel>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using MvvmCross.ViewModels;

namespace TestApp.Models
{
    public class Person : MvxViewModel
    {
        private string _firstName;
        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        private string _lastName;
        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);
        }

        private string _job;
        public string Job
        {
            get => _job;
            set => SetProperty(ref _job, value);
        }

        private string _image;
        public string Image
        {
            get => _image;
            set => SetProperty(ref _image, value);
        }

        private string _email;
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        private string _phone;
        public string Phone
        {
            get => _phone;
            set => SetProperty(ref _phone, value);
        }

        public List<ITransformation> Transformations => new List<ITransformation> { new CircleTransformation() };

        public Person(string firsnName, string lastName, string image, string job, string email, string phone)
        {
            FirstName = firsnName;
            LastName = lastName;
            Image = image;
            Email = email;
            Phone = phone;
            Job = job;
        }
    }
}

﻿using System;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using TestApp.Models;

namespace TestApp.ViewModels
{
    public class PersonDetailsViewModel: MvxViewModel<Person>
    {
        private readonly IMvxNavigationService _mvxNavigation;

        private Person _person;
        public Person Person => _person;

        public PersonDetailsViewModel(IMvxNavigationService mvxNavigation)
        {
            _mvxNavigation = mvxNavigation;
        }

        public override void Prepare(Person parameter)
        {
            _person = parameter;
        }
    }
}

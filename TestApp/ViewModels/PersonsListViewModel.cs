﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using TestApp.Models;
using TestApp.Services.Interfaces;


namespace TestApp.ViewModels
{
    public class PersonsListViewModel: MvxViewModel
    {
        private readonly IMvxNavigationService _mvxNavigation;
        private readonly IPersonsService _dataService;

        public PersonsListViewModel(IMvxNavigationService mvxNavigation, IPersonsService dataService)
        {
            _mvxNavigation = mvxNavigation;
            _dataService = dataService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
            Persons = _dataService.GetPersons();
        }

        private List<Person> _persons;
        public List<Person> Persons
        {
            get => _persons;
            set
            {
                _persons = value;
                RaisePropertyChanged(() => Persons);
            }
        }

        private MvvmCross.Commands.MvxCommand<Person> _personSelectedCommand;
        public ICommand PersonSelectedCommand => _personSelectedCommand = _personSelectedCommand 
            ?? new MvvmCross.Commands.MvxCommand<Person>(NavigateToPersonDetails);


        private void NavigateToPersonDetails(Person person)
        {
            _mvxNavigation.Navigate<PersonDetailsViewModel, Person>(person);
        }
    }
}
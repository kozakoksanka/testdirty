﻿using MvvmCross.Platforms.Ios.Core;

namespace TestApp.IOS
{
    public class Setup : MvxIosSetup<App>
    {
        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();

        }

        protected override void InitializeLastChance()
        {
            base.InitializeLastChance();
        }
    }
}
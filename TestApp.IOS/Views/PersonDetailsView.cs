﻿using FFImageLoading;
using FFImageLoading.Work;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using TestApp.ViewModels;
using UIKit;

namespace TestApp.IOS.Views
{
    public partial class PersonDetailsView : MvxViewController<PersonDetailsViewModel>
    {
        public PersonDetailsView() : base("PersonDetailsView", null)
        {
        }

        private string _imageURL;
        public string ImageURL
        {
            set
            {
                if (value != _imageURL)
                {
                    _imageURL = value;
                    UpdateContent();
                }
            }
            get { return _imageURL; }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Title = "Details";

            var set = this.CreateBindingSet<PersonDetailsView, PersonDetailsViewModel>();
            set.Bind().For(v => v.ImageURL).To(vm => vm.Person.Image);
            set.Bind(name_label).For(v => v.Text).To(vm => vm.Person.FirstName);
            set.Bind(job_label).For(v => v.Text).To(vm => vm.Person.Job);
            set.Bind(email_label).For(v => v.Text).To(vm => vm.Person.Email);
            set.Bind(phone_label).For(v => v.Text).To(vm => vm.Person.Phone);
            set.Apply();

            person_small_imageView.Layer.CornerRadius = 20;
            person_small_imageView.Layer.MasksToBounds = true;
        }

        protected void UpdateContent()
        {
            person_big_imageView.Image = null;
            person_small_imageView.Image = null;

            ImageService.Instance.LoadUrl(ImageURL)
                        .Into(person_big_imageView);

            ImageService.Instance.LoadUrl(ImageURL)
                        .Into(person_small_imageView);
        }
    }
}
﻿using System;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using TestApp.ViewModels;
using UIKit;

namespace TestApp.IOS.Views
{
    [MvxRootPresentation(WrapInNavigationController = true)]
    public partial class PersonsListView : MvxViewController<PersonsListViewModel>
    {
        public PersonsListView() : base("PersonsListView", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Title = "Persons";

            var source = new MvxSimpleTableViewSource(tableView, PersonCardView.Key, PersonCardView.Key);
            source.DeselectAutomatically = true;

            var set = this.CreateBindingSet<PersonsListView, PersonsListViewModel>();
            set.Bind(source).For(v => v.ItemsSource).To(vm => vm.Persons);
            set.Bind(source).For(v => v.SelectionChangedCommand).To(vm => vm.PersonSelectedCommand);
            set.Apply();

            tableView.Source = source;
        }
    }
}
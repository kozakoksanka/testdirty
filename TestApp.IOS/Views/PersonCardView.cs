﻿using System;
using FFImageLoading;
using FFImageLoading.Work;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using TestApp.Models;
using UIKit;

namespace TestApp.IOS.Views
{
    public partial class PersonCardView : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("PersonCardView");
        public static readonly UINib Nib;

        private string _imageURL;
        public string ImageURL
        {
            set
            {
                if (value != _imageURL)
                {
                    _imageURL = value;
                    UpdateContent();
                }
            }
            get { return _imageURL; }
        }

        static PersonCardView()
        {
            Nib = UINib.FromName("PersonCardView", NSBundle.MainBundle);
        }

        protected PersonCardView(IntPtr handle) : base(handle)
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<PersonCardView, Person>();
                set.Bind().For(v => v.ImageURL).To(vm => vm.Image);
                set.Bind(name_label).For(v => v.Text).To(vm => vm.FirstName);
                set.Bind(job_label).For(v => v.Text).To(vm => vm.Job);
                set.Apply();
            });
        }

        public override void MovedToSuperview()
        {
            base.MovedToSuperview();
            person_small_imageView.Layer.CornerRadius = 20;
            person_small_imageView.Layer.MasksToBounds = true;
        }

        protected void UpdateContent()
        {
            person_big_imageView.Image = null;
            person_small_imageView.Image = null;

            ImageService.Instance.LoadUrl(ImageURL)
                        .Into(person_big_imageView);

            ImageService.Instance.LoadUrl(ImageURL)
                        .Into(person_small_imageView);
        }
    }
}
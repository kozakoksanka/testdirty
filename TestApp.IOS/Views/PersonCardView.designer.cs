﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TestApp.IOS.Views
{
    [Register ("PersonCardView")]
    partial class PersonCardView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView contentView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel job_label { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel name_label { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView person_big_imageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView person_small_imageView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (contentView != null) {
                contentView.Dispose ();
                contentView = null;
            }

            if (job_label != null) {
                job_label.Dispose ();
                job_label = null;
            }

            if (name_label != null) {
                name_label.Dispose ();
                name_label = null;
            }

            if (person_big_imageView != null) {
                person_big_imageView.Dispose ();
                person_big_imageView = null;
            }

            if (person_small_imageView != null) {
                person_small_imageView.Dispose ();
                person_small_imageView = null;
            }
        }
    }
}